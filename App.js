import React, {Component} from 'react';
import {StatusBar, StyleSheet, Text, View} from 'react-native';
import {CButton} from './src/components';
import {colors, numberWithCommas} from './src/utils';

class App extends Component {
  constructor() {
    super();
    this.state = {
      number: '0',
      operation: null,
      prevValue: '0',
      newValue: false,
    };
  }

  inputNum = value => {
    const {number, isOperation, newValue} = this.state;
    if (newValue) {
      this.setState({
        number: `${value}`,
        newValue: false,
      });
    } else {
      if (number === '0') {
        this.setState({number: `${value}`});
      } else {
        this.setState(prevState => ({
          number: `${number}${value}`,
        }));
      }
    }
  };

  operation = opt => {
    const {number, prevValue, operation} = this.state;
    this.setState({
      operation: opt,
      newValue: true,
      prevValue: `${number}`,
    });
  };

  results = () => {
    const {number, prevValue, operation} = this.state;
    this.setState({
      number: eval(`${prevValue}${operation}${number}`),
    });
  };

  reset = () => {
    this.setState({
      number: '0',
      operation: null,
      prevValue: '0',
      newValue: false,
    });
  };

  addComa = () => {
    const {number} = this.state;
    if (number[number.length - 1] !== '.') {
      this.setState(prevState => ({
        number: prevState.number + '.',
      }));
    }
  };

  render() {
    const {number, prevValue, operation} = this.state;
    return (
      <View style={styles.container}>
        <StatusBar barStyle="dark-content" backgroundColor={colors.first} />

        <View style={styles.subContainer1}>
          <Text style={styles.number}>{numberWithCommas(number)}</Text>
        </View>

        <View style={styles.subContainer2}>
          <View style={styles.buttonContainer}>
            <View style={styles.rowButton}>
              <CButton onPress={() => this.inputNum(7)} title="7" />
              <CButton onPress={() => this.inputNum(8)} title="8" />
              <CButton onPress={() => this.inputNum(9)} title="9" />
            </View>
            <View style={styles.rowButton}>
              <CButton onPress={() => this.inputNum(4)} title="4" />
              <CButton onPress={() => this.inputNum(5)} title="5" />
              <CButton onPress={() => this.inputNum(6)} title="6" />
            </View>
            <View style={styles.rowButton}>
              <CButton onPress={() => this.inputNum(1)} title="1" />
              <CButton onPress={() => this.inputNum(2)} title="2" />
              <CButton onPress={() => this.inputNum(3)} title="3" />
            </View>
            <View style={styles.rowButton}>
              <CButton onPress={this.reset} title="C" />
              <CButton onPress={() => this.inputNum(0)} title="0" />
              <CButton onPress={this.addComa} title="." />
            </View>
          </View>

          <View style={styles.buttonContainer2}>
            <CButton
              type="OPERATION"
              onPress={() => this.operation('/')}
              title="/"
            />
            <CButton
              type="OPERATION"
              onPress={() => this.operation('*')}
              title="*"
            />
            <CButton
              type="OPERATION"
              onPress={() => this.operation('+')}
              title="+"
            />
            <CButton
              type="OPERATION"
              onPress={() => this.operation('-')}
              title="-"
            />
            <CButton type="OPERATION" onPress={this.results} title="=" />
          </View>
        </View>
      </View>
    );
  }
}

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.first,
  },
  subContainer1: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    paddingHorizontal: 10,
  },
  subContainer2: {
    flex: 1,
    backgroundColor: colors.first,
    flexDirection: 'row',
    padding: 8,
  },
  number: {
    color: colors.black,
    fontSize: 64,
    textAlign: 'right',
  },
  buttonContainer: {
    flex: 2,
  },
  rowButton: {
    flex: 1,
    flexDirection: 'row',
  },
  buttonContainer2: {
    flex: 1,
  },
});
