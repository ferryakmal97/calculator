import React, {memo} from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import {colors} from '../../utils';

const CButton = ({title, type = 'OTHER', onPress}) => {
  console.log('Button di-render');
  return (
    <TouchableOpacity
      style={styles.button(type)}
      activeOpacity={0.7}
      onPress={onPress}>
      <Text style={styles.textButton(type)}>{title}</Text>
    </TouchableOpacity>
  );
};

const compare = (prevProps, nextProps) => {
  return JSON.stringify(prevProps) === JSON.stringify(nextProps);
};

export default memo(CButton, compare);

const styles = StyleSheet.create({
  button: type => ({
    flex: 1,
    backgroundColor: type === 'OPERATION' ? colors.fourth : colors.third,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 8,
    elevation: 2,
  }),
  textButton: type => ({
    color: type === 'OPERATION' ? colors.first : colors.black,
    fontSize: 42,
  }),
});
