export const colors = {
  midBlack: '#181818',
  midBlack2: '#191919',
  orange: '#FAB162',
  blue: '#2CCCC3',
  yellow: '#FACD3D',
  purple: '#5626C4',
  pink: '#E60576',
  first: '#f9fafc',
  second: '#ebeef3',
  third: '#e1e8ef',
  fourth: '#b7c2ce',
  black: '#262626',
};
